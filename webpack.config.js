const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: './src/main/webapp/app/index.tsx',
    cache: true,
    output: {
        path: __dirname,
        filename: "./src/main/resources/static/built/bundle.js"
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [
            {
                test: /\.(ts|js)x?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                },
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/main/resources/static/index.html"
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, "src/main/resources/static/dev"),
        compress: true,
        port: 8092
    }
}