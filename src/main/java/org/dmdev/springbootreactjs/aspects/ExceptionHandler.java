package org.dmdev.springbootreactjs.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.utils.ErrorsGetter;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class ExceptionHandler {

    @Around("execution(* org.dmdev.springbootreactjs.repositories.*.*(..))")
    public Object onRepositoryException(ProceedingJoinPoint pjp) throws Exception {
        Object output = null;
        try {
            output = pjp.proceed();
        } catch (Exception e) {
            if (ErrorsGetter.getException(e).contains("ConstraintViolationException")) {
                throw new ConstraintViolationException("", null, "");
            }
            throw e;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return output;
    }

    public Object onServiceException(ProceedingJoinPoint pjp) throws Exception {
        Object output = null;
        try {
            output = pjp.proceed();
        } catch (ConstraintViolationException ex) {
            output =
                    ResponseModel.builder()
                            .status(ResponseModel.FAIL_STATUS)
                            .message("This name is already taken")
                            .build();
        } catch (Exception ex) {
            System.err.println("SQL error!");
            ex.printStackTrace();
            output =
                    ResponseModel.builder()
                            .status(ResponseModel.FAIL_STATUS)
                            .message("Unknown database error")
                            .build();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return output;
    }
}
