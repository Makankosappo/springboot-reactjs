package org.dmdev.springbootreactjs.repositories;

import org.dmdev.springbootreactjs.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryHibernateDAO extends JpaRepository<Category, Long> {
}
