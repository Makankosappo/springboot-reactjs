package org.dmdev.springbootreactjs.repositories.criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SearchCriteria {
    private String key; // entity name
    private String operation; // operation sing
    private Object value; // value for comparing
}
