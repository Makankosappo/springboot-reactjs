package org.dmdev.springbootreactjs.repositories.predicate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import org.apache.commons.lang3.StringUtils;
import org.dmdev.springbootreactjs.entities.Product;
import org.dmdev.springbootreactjs.repositories.criteria.SearchCriteria;

import java.util.Arrays;
import java.util.List;

public class ProductPredicate {

    private SearchCriteria criteria;

    public ProductPredicate(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    public BooleanExpression getPredicate() throws JsonProcessingException {
        PathBuilder<Product> entityPath =
                new PathBuilder<>(Product.class, "product");
        if(StringUtils.isNumeric(criteria.getValue().toString())) {
            NumberPath<Integer> path =
                    entityPath.getNumber(criteria.getKey(), Integer.class);
            int value = Integer.parseInt(criteria.getValue().toString());

            switch (criteria.getOperation()){
                case ":":
                    return path.eq(value);
                case ">":
                    return path.gt(value);
                case "<":
                    return path.lt(value);
                case ">:":
                    return path.goe(value);
                case "<:":
                    final int SQL_ROUND_COMPENSATION = 1;
                    return path.loe(value + SQL_ROUND_COMPENSATION);
            }
        } else if ( criteria.getValue().toString().startsWith("[")) {
            entityPath =
                    new PathBuilder<Product>(Product.class, criteria.getKey() + ".id");
            List value = Arrays.asList(
                    new ObjectMapper().readValue(
                            criteria.getValue().toString(),
                            Long[].class
                    )
            );
            return entityPath.in(value);
        } else {
            StringPath path = entityPath.getString(criteria.getKey());
            if(criteria.getOperation().equalsIgnoreCase(":")) {
                return path.containsIgnoreCase(criteria.getValue().toString());
            }
        }
        return null;
    }
}
