package org.dmdev.springbootreactjs.repositories.predicate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.dmdev.springbootreactjs.repositories.criteria.SearchCriteria;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductPredicatesBuilder {

    private List<SearchCriteria> params;

    public ProductPredicatesBuilder(){params = new ArrayList<>();}

    public ProductPredicatesBuilder with(
            String key, String operation, Object value)
    {
        params.add(new SearchCriteria(key,operation,value));
        return this;
    }

    public BooleanExpression build(){
        if(params.size() == 0){
            return null;
        }
        List<BooleanExpression> predicates = params.stream().map(param -> {
            ProductPredicate predicate = new ProductPredicate(param);
            try {
                return predicate.getPredicate();
            } catch ( JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }
        }).filter(Objects::nonNull).toList();

        BooleanExpression result = Expressions.asBoolean(true).isTrue();
        for (BooleanExpression predicate : predicates){
            result = result.and(predicate);
        }
        return result;
    }
}
