package org.dmdev.springbootreactjs.repositories;

import org.dmdev.springbootreactjs.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleHibernateDAO extends JpaRepository<Role,Long> {
    Role findRoleByName(String name);
}
