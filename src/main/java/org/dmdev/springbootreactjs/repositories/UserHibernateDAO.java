package org.dmdev.springbootreactjs.repositories;

import org.dmdev.springbootreactjs.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserHibernateDAO extends JpaRepository<User,Long> {
    User findUserByName(String name);
}
