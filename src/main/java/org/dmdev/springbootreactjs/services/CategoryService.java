package org.dmdev.springbootreactjs.services;

import org.dmdev.springbootreactjs.entities.Category;
import org.dmdev.springbootreactjs.models.CategoryModel;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.repositories.CategoryHibernateDAO;
import org.dmdev.springbootreactjs.repositories.ProductHibernateDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryHibernateDAO categoryDAO;

    @Autowired
    private ProductHibernateDAO productDAO;

    public ResponseModel create(CategoryModel categoryModel) {
        Category category = Category.builder()
                .name(categoryModel.getName().trim())
                .build();
        categoryDAO.save(category);
        System.out.printf("Category %s Created%n", category.getName());
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("Category %s Created", category.getName()))
                .build();
    }

    public ResponseModel update(CategoryModel categoryModel) {
        Category category = Category.builder()
                .id(categoryModel.getId())
                .name(categoryModel.getName().trim())
                .build();
        categoryDAO.save(category);
        System.out.println(String.format("Category %s Updated", category.getName()));
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("Category %s Updated", category.getName()))
                .build();
    }

    public ResponseModel getAll() {
        List<Category> categories = categoryDAO.findAll(Sort.by("id").descending());
        List<CategoryModel> categoryModels =
                categories.stream()
                        .map(c ->
                        CategoryModel.builder()
                                .id(c.getId())
                                .name(c.getName())
                                .build()
                ).toList();
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .data(categoryModels)
                .build();
    }

    public ResponseModel delete(Long id) {
        Optional<Category> categoryOptional = categoryDAO.findById(id);
        if (categoryOptional.isPresent()) {
            Category category = categoryOptional.get();
            if (productDAO.countProductByCategory(category) == 0) {
                categoryDAO.delete(category);
                return ResponseModel.builder()
                        .status(ResponseModel.SUCCESS_STATUS)
                        .message(String.format("Category #%s Deleted", category.getName()))
                        .build();
            } else {
                return ResponseModel.builder()
                        .status(ResponseModel.FAIL_STATUS)
                        .message(String.format("Can`t delete the category #%s. There are some products", category.getName()))
                        .build();
            }
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.FAIL_STATUS)
                    .message(String.format("Category #%d Not Found", id))
                    .build();
        }
    }


}
