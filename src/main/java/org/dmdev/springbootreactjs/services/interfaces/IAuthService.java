package org.dmdev.springbootreactjs.services.interfaces;


import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.models.RoleModel;
import org.dmdev.springbootreactjs.models.UserModel;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;


public interface IAuthService {

    public ResponseModel createRole(RoleModel roleModel);

    public ResponseModel createUser(UserModel userModel);

    public ResponseModel getAllRoles();

    @Transactional
    public ResponseModel getRoleUsers(Long roleId);

    public ResponseModel deleteRole (Long id);

    public ResponseModel deleteUser (Long id);

    public ResponseModel check (Authentication authentication);

    public ResponseModel onSingOut();

    public ResponseModel onError();

    public ResponseModel makeUserAdmin( Long id) throws Exception;
}
