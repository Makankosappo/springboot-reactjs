package org.dmdev.springbootreactjs.services.interfaces;

import org.dmdev.springbootreactjs.models.ProductFilterModel;
import org.dmdev.springbootreactjs.models.ProductModel;
import org.dmdev.springbootreactjs.models.ProductSearchModel;
import org.dmdev.springbootreactjs.models.ResponseModel;

public interface IProductService {
    ResponseModel create(ProductModel productModel);
    ResponseModel update(ProductModel productModel);
    ResponseModel getAll();
    ResponseModel delete(Long id);
    ResponseModel getFiltered(ProductFilterModel filter);
    ResponseModel search(ProductSearchModel searchModel);
}
