package org.dmdev.springbootreactjs.services.interfaces;

import org.dmdev.springbootreactjs.models.CategoryModel;
import org.dmdev.springbootreactjs.models.ResponseModel;

public interface ICategoryService {
    ResponseModel create(CategoryModel categoryModel);
    ResponseModel getAll();
    ResponseModel delete(Long id);
}
