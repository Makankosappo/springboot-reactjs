package org.dmdev.springbootreactjs.services;

import org.dmdev.springbootreactjs.entities.Role;
import org.dmdev.springbootreactjs.entities.User;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.models.RoleModel;
import org.dmdev.springbootreactjs.models.UserModel;
import org.dmdev.springbootreactjs.repositories.RoleHibernateDAO;
import org.dmdev.springbootreactjs.repositories.UserHibernateDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuthService {
    @Autowired
    private RoleHibernateDAO roleDAO;
    @Autowired
    private UserHibernateDAO userDAO;
    @Autowired
    public PasswordEncoder passwordEncoder;

    public ResponseModel createRole(RoleModel roleModel) {
        Role role = Role.builder().name(roleModel.name).build();
        roleDAO.save(role);
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("%s Role Created", role.getName()))
                .build();
    }

    public ResponseModel createUser(UserModel userModel) {
        User user = User.builder()
                .name(userModel.getName().trim())
                .password(userModel.getPassword())
                .role(roleDAO.findRoleByName("ROLE_USER"))
                .build();
        userDAO.save(user);
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("%s User Created", user.getName()))
                .build();
    }

    public ResponseModel getAllRoles() {
        List<Role> rolesList = roleDAO.findAll(Sort.by("name").ascending());
        List<RoleModel> roleModelsList = rolesList.stream().map((r) -> RoleModel.builder()
                .id(r.getId())
                .name(r.getName())
                .build())
                .toList();
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("Role List Retrieved Successfully"))
                .build();
    }

    @Transactional
    public ResponseModel getRoleUsers(Long roleId){
        Optional<Role> roleOptional = roleDAO.findById(roleId);
        if(roleOptional.isPresent()) {
            Role role = roleOptional.get();
            List<UserModel> userModels =
                    role.getUsers().stream().map(user ->
                            UserModel.builder()
                                    .name(user.getName())
                                    .roleId(user.getId())
                                    .build())
                            .toList();
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("List of %s Role Users Retrieved Successfully", role.getName()))
                    .build();
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.FAIL_STATUS)
                    .message(String.format("No Users: Role #%d Not Found", roleId))
                    .build();
        }
    }

    public ResponseModel deleteRole (Long id) {
        roleDAO.deleteById(id);
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("Role Deleted"))
                .build();
    }

    public ResponseModel deleteUser (Long id) {
        userDAO.deleteById(id);
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("User Deleted"))
                .build();
    }

    public ResponseModel check (Authentication authentication) {
        ResponseModel responseModel = new ResponseModel();
        if(authentication != null && authentication.isAuthenticated()) {
            UserModel userModel = UserModel.builder()
                    .name(authentication.getName())
                    .roleName(authentication.getAuthorities().stream()
                            .findFirst()
                            .get()
                            .getAuthority()
                    )
                    .build();
            responseModel.setStatus(ResponseModel.SUCCESS_STATUS);
            responseModel.setMessage(String.format("User %s Singed in", userModel.getName()));
            responseModel.setData(userModel);
        } else {
            responseModel.setStatus(ResponseModel.SUCCESS_STATUS);
            responseModel.setMessage("User is a Guest");
        }
        return responseModel;
    }

    public ResponseModel onSingOut() {
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message("Singed out")
                .build();
    }

    public ResponseModel onError() {
        return ResponseModel.builder()
                .status(ResponseModel.FAIL_STATUS)
                .message("Auth Error")
                .build();
    }

    public ResponseModel makeUserAdmin( Long id) throws Exception {
        Role role = roleDAO.findRoleByName("ROLE_ADMIN");
        Optional<User> userOptional = userDAO.findById(id);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            user.setRole(role);
            userDAO.save(user);
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Admin %s created successfully", user.getName()))
                    .build();
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.FAIL_STATUS)
                    .message(String.format("User #%d Not Found", id))
                    .build();
        }
    }
}
