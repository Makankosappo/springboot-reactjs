package org.dmdev.springbootreactjs.services;

import com.querydsl.core.types.dsl.BooleanExpression;
import org.dmdev.springbootreactjs.entities.Category;
import org.dmdev.springbootreactjs.entities.Product;
import org.dmdev.springbootreactjs.models.*;
import org.dmdev.springbootreactjs.repositories.CategoryHibernateDAO;
import org.dmdev.springbootreactjs.repositories.ProductHibernateDAO;
import org.dmdev.springbootreactjs.repositories.predicate.ProductPredicatesBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductHibernateDAO productDAO;

    @Autowired
    private CategoryHibernateDAO categoryDAO;

    public ResponseModel create(ProductModel productModel) {
        Optional<Category> categoryOptional
                = categoryDAO.findById(productModel.getId());
        if (categoryOptional.isPresent()) {
            Product product = Product.builder()
                    .name(productModel.getTitle())
                    .description(productModel.getDescription())
                    .price(productModel.getPrice())
                    .quantity(productModel.getQuantity())
                    .image(productModel.getImage())
                    .category(categoryOptional.get())
                    .build();
            productDAO.save(product);
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Product %s Created", product.getName()))
                    .build();
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.FAIL_STATUS)
                    .message(String.format("Category #%d Not Found", productModel.getCategoryId()))
                    .build();
        }
    }

    public ResponseModel update(ProductModel productModel) {
        Optional<Category> categoryOptional
                = categoryDAO.findById(productModel.getId());
        if (categoryOptional.isPresent()) {
            Product product = Product.builder()
                    .name(productModel.getTitle())
                    .description(productModel.getDescription())
                    .price(productModel.getPrice())
                    .quantity(productModel.getQuantity())
                    .image(productModel.getImage())
                    .category(categoryOptional.get())
                    .build();
            productDAO.save(product);
            System.out.println(String.format("Product %s Updated", product.getName()));
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Product %s Updated", product.getName()))
                    .build();
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.FAIL_STATUS)
                    .message(String.format("Category #%d Not Found", productModel.getCategoryId()))
                    .build();
        }
    }

    public ResponseModel getAll() {
        List<Product> products = productDAO.findAll(Sort.by("id").descending());
        List<ProductModel> productModels =
                products.stream().map(p ->
                                ProductModel.builder()
                                        .id(p.getId())
                                        .quantity(p.getQuantity())
                                        .title(p.getName())
                                        .price(p.getPrice())
                                        .description(p.getDescription())
                                        .image(p.getImage())
                                        .category(
                                                CategoryModel.builder()
                                                        .id(p.getCategory().getId())
                                                        .name(p.getCategory().getName())
                                                        .build()
                                        )
                                        .build()
                        )
                        .toList();
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .data(productModels)
                .build();
    }

    public ResponseModel delete (Long id) {
        Optional<Product> productOptional = productDAO.findById(id);
        if(productOptional.isPresent()){
            Product product = productOptional.get();
            productDAO.delete(product);
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Product %s Deleted", product.getName()))
                    .build();
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.FAIL_STATUS)
                    .message(String.format("Product #%d Not Found", id))
                    .build();
        }
    }

    public ResponseModel getFiltered(ProductFilterModel filter) {
        List<Product> products =
                productDAO.findByCategoryIds(filter.categories,
                        Sort.by(filter.sortingDirection, filter.orderBy)
                );
        return getResponseModelFromEntities(products);
    }

    public ResponseModel search(ProductSearchModel searchModel) {
        List<Product> products = null;
        if(searchModel.searchString != null && searchModel.searchString.isEmpty()) {
            ProductPredicatesBuilder builder = new ProductPredicatesBuilder();
            Pattern pattern = Pattern.compile("([\\w]+?)(:|<|>|<:|>:)([\\W\\]\\[\\,]+?);");
            Matcher matcher = pattern.matcher(searchModel.searchString + ";");
            while(matcher.find()){
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }
            BooleanExpression expression = builder.build();
            products =
                    (List<Product>) productDAO.findAll(
                            expression,
                            Sort.by(
                                    searchModel.sortingDirection,
                                    searchModel.orderBy
                            )
                    );
        } else {
            products =
                    productDAO.findAll(
                            Sort.by(
                                    searchModel.sortingDirection,
                                    searchModel.orderBy
                            )
                    );
        }
        return getResponseModelFromEntities(products);
    }

    private ResponseModel getResponseModelFromEntities(List<Product> products){
        List<ProductModel> productModels =
            products.stream().map(p ->
                    ProductModel.builder()
                            .id(p.getId())
                            .title(p.getName())
                            .description(p.getDescription())
                            .price(p.getPrice())
                            .quantity(p.getQuantity())
                            .image(p.getImage())
                            .category(
                                    CategoryModel.builder()
                                            .id(p.getCategory().getId())
                                            .name(p.getCategory().getName())
                                            .build()
                            )
                            .build())
                    .toList();
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .data(productModels)
                .build();
    }
}
