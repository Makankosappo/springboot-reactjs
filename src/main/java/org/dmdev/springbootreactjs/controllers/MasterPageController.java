package org.dmdev.springbootreactjs.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MasterPageController {

    @RequestMapping(value = {
            "/",
            "/shopping",
            "/singin",
            "/singup",
            "/about",
            "/admin"
    })
    public String index() {return "index.html";}
}
