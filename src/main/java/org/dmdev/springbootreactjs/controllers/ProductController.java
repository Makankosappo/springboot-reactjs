package org.dmdev.springbootreactjs.controllers;

import org.dmdev.springbootreactjs.models.ProductFilterModel;
import org.dmdev.springbootreactjs.models.ProductModel;
import org.dmdev.springbootreactjs.models.ProductSearchModel;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<ResponseModel> getAll() {
        return new ResponseEntity<>(productService.getAll(), HttpStatus.OK);
    }

    @PostMapping("/products")
    public ResponseEntity<ResponseModel> create(@RequestBody ProductModel product) {
        return new ResponseEntity<>(productService.create(product), HttpStatus.CREATED);
    }

    @PatchMapping("/products/{id}")
    public ResponseEntity<ResponseModel> update(@RequestParam Long id, @RequestBody ProductModel product) {
        product.setId(id);
        return new ResponseEntity<>(productService.update(product), HttpStatus.OK);
    }

    @GetMapping("/categories/{categoryIds}/products::orderBy:{orderBy}::sortingDirection:{sortingDirection}")
    public ResponseEntity<ResponseModel> getByCategories(
            @PathVariable List<Long> categoryIds,
            @PathVariable String orderBy,
            @PathVariable Sort.Direction sortingDirection
    ) {
        return new ResponseEntity<>(
                productService.getFiltered(
                        new ProductFilterModel(categoryIds, orderBy, sortingDirection)
                ),
                HttpStatus.OK
        );
    }

    @GetMapping("/products/filtered::orderBy:{orderBy}::sortingDirection:{sortingDirection}")
    public ResponseEntity<ResponseModel> search(
            @RequestParam(value = "search") String searchString,
            @PathVariable String orderBy,
            @PathVariable Sort.Direction sortingDirection
    ) {
        return new ResponseEntity<>(
                productService.search(
                        new ProductSearchModel(searchString, orderBy, sortingDirection)
                ),
                HttpStatus.OK
        );
    }
}
