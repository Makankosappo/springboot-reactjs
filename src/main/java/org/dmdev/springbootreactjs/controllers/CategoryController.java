package org.dmdev.springbootreactjs.controllers;

import org.dmdev.springbootreactjs.models.CategoryModel;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories")
    public ResponseEntity<ResponseModel> getAll() {
        return new ResponseEntity<>(categoryService.getAll(), HttpStatus.OK);
    }

    @PostMapping("/categories")
    public ResponseEntity<ResponseModel> create(@RequestBody CategoryModel category){
        return new ResponseEntity<>(categoryService.create(category), HttpStatus.CREATED);
    }

    @PatchMapping("/categories/{id}")
    public ResponseEntity<ResponseModel> update(@PathVariable Long id, @RequestBody CategoryModel category){
        category.setId(id);
        return new ResponseEntity<>(categoryService.update(category), HttpStatus.OK);
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<ResponseModel> delete(@PathVariable Long id){
        ResponseModel responseModel = categoryService.delete(id);
        System.out.println(responseModel);
        return new ResponseEntity<>(responseModel, HttpStatus.OK);
    }

}
