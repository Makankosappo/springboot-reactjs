package org.dmdev.springbootreactjs.controllers;

import org.dmdev.springbootreactjs.entities.Role;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.models.RoleModel;
import org.dmdev.springbootreactjs.models.UserModel;
import org.dmdev.springbootreactjs.repositories.RoleHibernateDAO;
//import org.dmdev.springbootreactjs.services.RoleService;
import org.dmdev.springbootreactjs.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @Secured("ROLE_ADMIN")
    @GetMapping("/admin/roles")
    public ResponseEntity<ResponseModel> getAllRoles() {
        return new ResponseEntity<>(authService.getAllRoles(), HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/admin/roles")
    public ResponseEntity<ResponseModel> createRole(@RequestBody RoleModel roleModel) {
        return new ResponseEntity<>(authService.createRole(roleModel), HttpStatus.CREATED);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/admin/roles/{id}")
    public ResponseEntity<ResponseModel> deleteRole(@PathVariable Long id) {
        return new ResponseEntity<>(authService.deleteRole(id), HttpStatus.NO_CONTENT);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/admin/roles/{id}")
    public ResponseEntity<ResponseModel> getUserByRole(@PathVariable Long id) {
        ResponseModel responseModel = authService.getRoleUsers(id);
        return new ResponseEntity<>(responseModel,
                (responseModel.getData() != null)
                        ? HttpStatus.OK
                        : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<ResponseModel> createUser(@RequestBody UserModel userModel) {
        ResponseModel responseModel = authService.createUser(userModel);
        return new ResponseEntity<>(responseModel,
                (responseModel.getMessage().toLowerCase().contains("created"))
                ? HttpStatus.CREATED
                :responseModel.getMessage().contains("name")
                ? HttpStatus.CONFLICT
                : HttpStatus.BAD_GATEWAY
        );
    }

    @DeleteMapping(value = "/user/{id}")
    public ResponseEntity<ResponseModel> deleteUser (@PathVariable Long id) {
        return new ResponseEntity<>(authService.deleteUser(id), HttpStatus.NO_CONTENT);
    }


    @GetMapping("/user/check")
    @ResponseBody
    public ResponseEntity<ResponseModel> checkUser(Authentication authentication) {
        ResponseModel responseModel = authService.check(authentication);
        return new ResponseEntity<>(responseModel,
                (responseModel.getData() != null)
                        ? HttpStatus.OK
                        : HttpStatus.UNAUTHORIZED);
    }

    @Secured("ROLE_ADMIN")
    @PatchMapping(value = "/users/{id}/makeadmin")
    public ResponseEntity<ResponseModel> makeUserAdmin(@PathVariable Long id) throws Exception {
        return new ResponseEntity<>(authService.makeUserAdmin(id), HttpStatus.OK);
    }

}
