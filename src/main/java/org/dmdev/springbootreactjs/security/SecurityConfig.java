package org.dmdev.springbootreactjs.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
//@EnableWebMvc
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private HibernateWebAuthProvider hibernateWebAuthProvider;

    @Autowired
    private SavedReqAwareAuthSuccessHandler savedReqAwareAuthSuccessHandler;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        return http
                .authenticationProvider(hibernateWebAuthProvider)
                .authorizeHttpRequests(auth -> {
                    auth
                            .requestMatchers(HttpMethod.GET, "/", "/*.html", "/**.html", "/**.css", "/*.js", "/**.js", "/**", "/**.png").permitAll()
                            .requestMatchers(HttpMethod.GET, "/api/auth/user/**").permitAll()
                            .requestMatchers(HttpMethod.POST, "/api/auth/users").permitAll()
                            .requestMatchers(HttpMethod.POST, "/api/auth/user/**").permitAll()
                            .requestMatchers(HttpMethod.DELETE, "/api/auth/user/**").authenticated()
                            .requestMatchers("/api/auth/roles").permitAll()
                            .requestMatchers(HttpMethod.GET, "/api/auth/role/**").permitAll()
                            .requestMatchers("/api/cart/**").authenticated()
                            .requestMatchers("/shared/**").permitAll()
                            .requestMatchers("/admin/**").hasRole("ADMIN")
                            .requestMatchers("/api/**/admin/**").hasRole("ADMIN");
                    auth.anyRequest().authenticated();
                })
                .httpBasic(Customizer.withDefaults())
                .formLogin(form -> form
                        .successHandler(savedReqAwareAuthSuccessHandler)
                        .permitAll())
                .build();
    }
}
