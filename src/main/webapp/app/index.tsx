import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import {Provider} from "mobx-react"

import commonStore from './store/CommonStore'
import userStore from './store/UserStore'
import routerStore from "./store/RouterStore";
import categoryStore from "./store/CategoryStore";

const stores = {
    commonStore,
    userStore,
    routerStore,
    categoryStore,
}

ReactDOM.render(
    <Provider {...stores}>
        <App />
    </Provider>,
    document.getElementById('root')
)
