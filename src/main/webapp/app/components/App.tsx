/*
import React from 'react'
import {observer} from 'mobx-react'
import commonStore from "app/store/CommonStore";

export default observer( () => {
    setTimeout(() => commonStore.setError('Demo Error message'),
            5000
        )
    return(
        <>
            <h1>Test</h1>
            <span>Error: {commonStore.error}</span>
        </>
    )
})
*/

import React, {Component} from 'react';
import {CommonStore} from 'app/store/CommonStore';
import {RouterStore} from 'app/store/RouterStore';
import history from "../history";
import {BrowserRouter} from "react-router-dom";
import {Router, Route} from 'react-router-dom';
import {Toolbar, AppBar, Container, Typography, withStyles, WithStyles} from "@material-ui/core";
import {inject, observer} from "mobx-react";
import {CSSTransition} from "react-transition-group";
import {UserStore} from "app/store/UserStore";
import AppBarCollapse from "app/components/common/AppBarCollapse";

interface IProps extends WithStyles<typeof styles>{
    commonStore: CommonStore
    routerStore: RouterStore
    userStore: UserStore
}

interface IState {
}


const styles = theme =>
    ({
    root: {
        flexGrow: 1,
    },
    container: {
        maxWidth: '970px',
        '& .page' : {
            position: 'static'
        }
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    navBar: {
        color: '#fff',
        backgroundColor: '#ee6e73'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalContent: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    cartModalContent: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3)
    },
    closeButton: {
        cursor: 'pointer',
        //float: 'right',
        marginTop: '-80px',
        marginRight: '-25px',
    }
})

@inject('commonStore', 'routerStore', 'userStore')
@observer
class App extends Component<IProps, IState> {

    constructor(props) {
        super(props);
    }

    render() {
        const {routes} = this.props.routerStore
        const {classes} = this.props
        return (
            <Router history={history}>
                <div className={classes.root}>
                    <AppBar>
                        <Toolbar>
                            <Typography variant='h6' className={classes.title}>
                               ReactSPA
                            </Typography>
                            <AppBarCollapse routes={routes} />
                        </Toolbar>
                    </AppBar>
                    <Container maxWidth="sm" className={classes.container}>
                        {routes.map(({path, Component}) => (
                            <Route key={path} exact path={path}>
                                {({match}) => (
                                    <CSSTransition
                                        in={match != null}
                                        timeout={300}
                                        className='page'
                                        unmountOnExit
                                    >
                                        <div className='page '>
                                            <Component/>
                                        </div>
                                    </CSSTransition>
                                )}
                            </Route>
                        ))}
                    </Container>
                </div>
            </Router>
        );
    }
}

export default withStyles(styles)(App)
