import {UserStore} from "../../store/UserStore";
import {MenuItem, withStyles, WithStyles} from "@material-ui/core";
import ButtonAppBarCollapse from "./ButtonAppBarCollapse";
import RouteModel from "app/models/RouteModel";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import {inject, observer} from "mobx-react";


interface IProps extends WithStyles<typeof styles> {
    routes: Array<RouteModel>
    userStore: UserStore
}

interface IState {
}

const styles = theme => ({
    root: {
        //position: "absolute",
        right: 0,
    },
    buttonBar: {
        [theme.breakpoints.down("xs")]: {
            display: "none"
        },
        margin: "10px",
        paddingLeft: "16px",
        right: "10px",
        //position: "relative",
        width: "100%",
        background: "transparent",
        display: "inline",
    },
    buttonBarItem: {
        webkitTransition: 'background-color .3s',
        transition: 'background-color .3s',
        fontSize: '1rem',
        color: '#fff',
        padding: '15px',
        cursor: 'pointer',
        textDecoration: 'none'
    },
    buttonBarItemActive: {
        background: '#ea454b',
    },
    mobileButtonBarItem: {
        textDecoration: 'none',
    },
    mobileButtonBarItemActive: {
        backgroundColor: '#ccc',
    },
    shoppingCart: {
        marginRight: '10px',
    },
})

@inject('userStore')
@observer
class AppBarCollapse extends Component<IProps, IState> {
    constructor(props) {
        super(props)
    }

    render() {
        const {classes} = this.props
        const {routes} = this.props
        return (
            <div className={classes.root}>
                <ButtonAppBarCollapse>
                    {routes.map(route => {
                        if (!/^Dashboard[A-z]+$/.test(route.name)) {
                            return <MenuItem>
                                <NavLink
                                    key={route.path}
                                    as={NavLink}
                                    to={route.path}
                                    className={classes.mobileButtonBarItem}
                                    activeClassName={classes.mobileButtonBarItemActive}
                                    exact>
                                    {route.name}
                                </NavLink>
                            </MenuItem>
                        } else {
                            return ''
                        }
                    })}
                </ButtonAppBarCollapse>
                <div className={classes.buttonBar} id="appbar-collapse">
                    {routes.map(route => {
                        if (!/^Dashboard[A-z]+$/.test(route.name)) {
                            return <MenuItem>
                                <NavLink
                                    key={route.path}
                                    as={NavLink}
                                    to={route.path}
                                    className={classes.mobileButtonBarItem}
                                    activeClassName={classes.mobileButtonBarItemActive}
                                    exact>
                                    {route.name}
                                </NavLink>
                            </MenuItem>
                        } else {
                            return ''
                        }
                    })}
                </div>
            </div>
        )
    }
}

export default withStyles(styles)(AppBarCollapse)
