import {Button, Card, CardContent, Grid, Icon, TextField, withStyles, WithStyles} from "@material-ui/core";
import {CommonStore} from "app/store/CommonStore";
import {UserStore} from "app/store/UserStore";
import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router-dom";

interface IProps extends WithStyles<typeof styles> {
    commonStore: CommonStore
    userStore: UserStore
}

interface IState {
}

const styles = theme =>
    ({
        root: {
            '& > *': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
        singInGrid: {
            minHeight: '100vh'
        },
        card: {
            width: 275
        },
    })

@inject("commonStore", "userStore")
@withRouter
@observer
class SingIn extends Component<IProps, IState> {
    componentWillUnmount() {
        this.props.userStore.reset()
    }

    handleUserNameChange = e => {
        this.props.userStore.setUserName(e.target.value)
    }

    handlePasswordChange = e => {
        this.props.userStore.setPassword(e.target.value)
    }

    handleSubmitForm = e => {
        e.preventDefault()
        this.props.userStore.register()
    }

    render() {
        const {loading} = this.props.commonStore
        const {userName, password} = this.props.userStore
        const {classes} = this.props

        return (
            <Grid container
                  spacing={0}
                  direction='column'
                  alignContent='center'
                  justify='center'
                  className={classes.singInGrid}>
                <Grid item
                      xs={12}
                      sm={12}
                      md={3}
                      lg={3}
                      xl={3}>
                    <Card className={classes.card}>
                        <CardContent>
                            <form
                                className={classes.root}
                                noValidate
                                autoComplete="off"
                                title="Register"
                            >
                                <div>
                                    <TextField
                                        label='Login'
                                        value={userName}
                                        onChange={this.handleUserNameChange}
                                    />
                                </div>
                                <div>
                                    <TextField
                                        label='Password'
                                        value={password}
                                        type="password"
                                        onChange={this.handlePasswordChange}
                                    />
                                </div>
                                <div>
                                    <Button
                                        variant='outlined'
                                        disabled={loading}
                                        onClick={this.handleSubmitForm}
                                    >
                                        Submit
                                        <Icon>
                                            send
                                        </Icon>
                                    </Button>
                                </div>
                            </form>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(SingIn)