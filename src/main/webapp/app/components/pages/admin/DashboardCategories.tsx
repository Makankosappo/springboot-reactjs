import React, {Component} from "react";
import {CommonStore} from "app/store/CommonStore";
import {CategoryStore} from "app/store/CategoryStore";
import {inject, observer} from "mobx-react";
import {Button, Drawer, Icon, Table, TextField, withStyles, WithStyles} from "@material-ui/core";


interface IProps extends WithStyles<typeof styles> {
    commonStore: CommonStore
    categoryStore: CategoryStore
}

interface IState {
    formMode: string,
    sidePanelVisibility: boolean
}

const styles = theme => ({
    title: {
        display: 'inline',
        marginRight: 15
    },
    categoriesTableColumnHeader: {
        '& > th': {
            textAlign: 'left'
        }
    }
})

@inject("commonStore", "categoryStore")
@observer
class DashboardCategories extends Component<IProps, IState>{
    constructor(props){
        super(props)
        this.state = {
            formMode: 'add',
            sidePanelVisibility: false
        }
    }
    componentDidMount() {
        this.props.categoryStore.fetchCategories()
    }
    toggleDrawer = (open: boolean) => (
        event: React.KeyboardEvent | React.MouseEvent,
    ) => {
        if (
            event.type === 'keydown' &&
            ((event as React.KeyboardEvent).key === 'Tab' ||
                (event as React.KeyboardEvent).key === 'Shift')
        ) {
            return;
        }
        this.setState({sidePanelVisibility: open})
    }
    handleCategoryNameChange = e => {
        this.props.categoryStore.setCategoryName(e.target.value)
    }
    handleCategoryAdd = (e) => {
        this.setState({formMode: 'add'})
        this.setState({sidePanelVisibility: true})
    }
    handleCategoryEdit = (e, categoryId) => {
        this.setState({formMode: 'add'})
        this.setState({sidePanelVisibility: true})
        const currentCategory =
            this.props.categoryStore.categories.find(c => c.id === categoryId)
        this.props.categoryStore.setCurrentCategory(currentCategory)
    }
    handleCategoryDelete = (e, categoryId) => {
        this.props.categoryStore.setCurrentCategoryId(categoryId)
        this.props.categoryStore.deleteCategory()
    }
    handleSubmitForm = e => {
        e.preventDefault()
        this.setState({sidePanelVisibility: false})
        if(this.state.formMode === 'add') {
            this.props.categoryStore.add()
        } else {
            this.setState({formMode: 'add'})
            this.props.categoryStore.update()
        }
    }
    render() {
        const { loading } = this.props.commonStore
        const { categories } = this.props.categoryStore
        const { classes } = this.props
        return <div>
            <h2 className={classes.title}>Categories</h2>
            <Button
                variant='outlined'
                disabled={loading}
                onClick={this.handleCategoryAdd}
            >
            Add
               <Icon>
                   add
               </Icon>
            </Button>
            <Drawer
                open={this.state.sidePanelVisibility} onClose={this.toggleDrawer(false)}>
                <form>
                    <div>
                        <TextField
                            id="name"
                            label={'category name'}
                            value={this.props.categoryStore.currentCategory.name}
                            onChange={this.handleCategoryNameChange}
                        />
                    </div>
                    <div>
                        <Button
                            disabled={loading}
                            onClick={this.handleSubmitForm}
                        >
                            Submit
                            <Icon>
                                send
                            </Icon>
                        </Button>
                    </div>
                </form>
            </Drawer>
            <Table>
                <thead>
                <tr className={classes.categoriesTableColumnHeader}>
                    <th data-field="id">ID</th>
                    <th data-field="name">Name</th>
                </tr>
                </thead>
                {categories.map(category => {
                    return (
                        <tr>
                            <td>{category.id}</td>
                            <td>{category.name}</td>
                            <td>
                                <div>
                                    <Button
                                        onClick={(e) => {
                                            this.handleCategoryEdit(e, category.id)
                                        }}>
                                        <Icon>edit</Icon>
                                    </Button>
                                    <Button
                                        onClick={(e) => {
                                            this.handleCategoryDelete(e, category.id)
                                        }}>
                                        <Icon>delete</Icon>
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    )
                })}
            </Table>
        </div>
    }
}

export default withStyles(styles)(DashboardCategories)