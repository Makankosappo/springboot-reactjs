import React, {Component} from "react";
import Resizer from 'react-image-file-resizer'
import {
    Button,
    Drawer,
    FormControl,
    Icon,
    InputLabel,
    MenuItem,
    Select, Table,
    TextField, withStyles,
    WithStyles
} from "@material-ui/core";
import routerStore from "app/store/RouterStore";
import userStore from "app/store/UserStore";
import {CommonStore} from "app/store/CommonStore";
import {ProductStore} from "app/store/ProductStore";
import {CategoryStore} from "app/store/CategoryStore";
import {inject, observer} from "mobx-react";
import {reaction} from "mobx";

interface IProps extends WithStyles<typeof styles> {
    commonStore: CommonStore
    productStore: ProductStore
    categoryStore: CategoryStore
}

interface IState {
    formMode: string
    sidePanelVisibility: boolean
}

const styles = theme => ({
    title: {
        display: 'inline',
        marginRight: 15
    },
    productsTableColumnHeader: {
        '& > th': {
            textAlign: 'left'
        }
    },
    formControl: {
        margin: theme.spacing(1),
        display: 'block'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    form: {
        margin: 10
    },
    imageTextField: {
        display: 'none'
    },
    errorBlock: {
        right: 0,
        fontSize: '12px',
        color: 'red300',
//      position: 'absolute',
        marginTop: '-25px',
    },
})

@inject('commonStore', 'productStore', 'categoryStore')
@observer
class DashboardProducts extends Component<IProps, IState> {
    constructor(props) {
        super(props)
        this.state = {
            formMode: 'add',
            sidePanelVisibility: false,
        }
    }

    titleRef = React.createRef()

    componentDidMount() {
        this.props.categoryStore.fetchCategories()
        this.props.productStore.fetchProducts()
    }

    toggleDrawer = (open: boolean) => (
        event: React.KeyboardEvent | React.MouseEvent,
    ) => {
        if (
            event.type === 'keydown' &&
            ((event as React.KeyboardEvent).key === 'Tab' ||
                (event as React.KeyboardEvent).key === 'Shift')
        ) {
            return;
        }
        this.setState({sidePanelVisibility: open})
    }

    handleProductTitleChange = e => {
        this.props.productStore.setProductTitle(e.target.value)
    }

    handleProductDescriptionChange = e => {
        this.props.productStore.setProductDescription(e.target.value)
    }

    handleProductCategoryChange = e => {
        this.props.productStore.setProductCategory(e.target.value)
        document.getElementById('productCategortValidator').setAttribute('value', e.target.value)
    }

    handleProductPriceChange = e => {
        this.props.productStore.setProductPrice(e.target.value)
    }

    handleProductQuantityChange = e => {
        this.props.productStore.setProductQuantity(e.target.value)
    }

    handleProductImageChange = e => {
        const file = e.target.files[0]
        this.resizeFile(file).then((image: string) => {
            this.props.productStore.setProductImage(image)
        })
    }

    handleProductAdd = e => {
        this.setState({formMode: 'add'})
        this.setState({sidePanelVisibility: true})
    }

    handleProductEdit = (e, productId) => {
        this.props.productStore.setCurrentProductId(productId)
        this.setState({formMode: 'edit'})
        this.setState({sidePanelVisibility: true})
        const currentProduct =
            this.props.productStore.products.find(p => p.id === productId)
        this.props.productStore.setCurrentProduct(currentProduct)
    }

    handleProductDelete = (e, productId) => {
        this.props.productStore.setCurrentProductId(productId)
        this.props.productStore.deleteProduct()
    }

    handleBlur = e => {
        console.log(e.target.name + 'Ref')
        console.log(this[e.target.name + 'Ref'].current)
        this[e.target.name + 'Ref'].current.validate(e.target.value)
    }

    handleSubmitForm = e => {
        e.preventDefault()
        this.setState({sidePanelVisibility: false})
        if (this.state.formMode === 'add') {
            this.props.productStore.add()
        } else {
            this.setState({formMode: 'add'})
            this.props.productStore.update()
        }
    }

    resizeFile = (file) => new Promise(resolve => {
        Resizer.imageFileResizer(file, 300, 300, 'JPEG', 100, 0,
            uri => {
                resolve(uri);
            }, 'base64'
        )
    })

    imageReaction = reaction(
        () => this.props.productStore.currentProductImage,
        (image) => {
            document.getElementById('productImage').setAttribute('src', image)
        }
    )


    render() {
        const {loading} = this.props.commonStore
        const {products} = this.props.productStore
        const {categories} = this.props.categoryStore
        const {classes} = this.props

        return <div>
            <h2 className={classes.title}>Products</h2>
            <Button
                variant='outlined'
                disabled={loading}
                onClick={this.handleProductAdd}
            >
                Add
                <Icon>
                    add
                </Icon>
            </Button>
            <Drawer>
                open={this.state.sidePanelVisibility} onClose={this.toggleDrawer(false)}
                <ValidatorForm
                    className={classes.form}
                    onSubmit={this.handleSubmitForm}
                    onError={errors => console.log(errors)}
                    ref='form'
                >
                    <FormControl
                        className={classes.formControl}
                    >
                        <TextField
                            id='title'
                            name='title'
                            label={'product title'}
                            value={this.props.productStore.currentProduct.title}
                            onChange={this.handleProductTitleChange}
                            required
                        />
                    </FormControl>
                    <FormControl
                        className={classes.formControl}
                    >
                        <InputLabel id="category-label">category</InputLabel>
                        <Select
                            id="category"
                            labelId="category-label"
                            value={this.props.productStore.currentProduct.categoryId}
                            onChange={this.handleProductCategoryChange}
                        >
                            {categories.map(category => {
                                return (
                                    <MenuItem
                                        value={category.id.toString()}
                                    >
                                        {category.name}
                                    </MenuItem>
                                )
                            })}
                        </Select>
                        <input
                            id='productCategoryValidator'
                            tabIndex={-1}
                            autoComplete="off"
                            style={{opacity: 0, height: 0}}
                            value={this.props.productStore.currentProduct.categoryId?.toString()}
                            required={true}
                        />
                    </FormControl>
                    <FormControl
                        className={classes.formControl}
                    >
                        <TextField
                            id='description'
                            name='description'
                            label={'product description'}
                            value={this.props.productStore.currentProduct.description}
                            onChange={this.handleProductDescriptionChange}
                        />
                    </FormControl>
                    <FormControl
                        className={classes.formControl}
                    >
                        <TextField
                            id='quantity'
                            name='quantity'
                            label={'product quantity'}
                            type='number'
                            inputProps={{'min': 0}}
                            value={this.props.productStore.currentProduct.quantity}
                            onChange={this.handleProductQuantityChange}
                        />
                    </FormControl>
                    <FormControl
                        className={classes.formControl}
                    >
                        <TextField
                            id='price'
                            name='price'
                            label={'product price'}
                            type='number'
                            value={this.props.productStore.currentProduct.price}
                            onChange={this.handleProductPriceChange}
                            required
                            inputProps={{pattern: '[0-9]*[.]?[0-9]+'}}
                        />
                    </FormControl>
                    <FormControl
                        className={classes.formControl}
                    >
                        <div>
                            <div>
                                <img id="productImage"/>
                            </div>
                            <div>
                                <Button
                                    variant='contained'
                                    component="label"
                                >
                                    <div>
                                        image
                                        <TextField
                                            id="image"
                                            type='file'
                                            className={classes.imageTextField}
                                            onChange={this.handleProductImageChange}
                                        >

                                        </TextField>
                                    </div>
                                </Button>
                            </div>
                        </div>
                    </FormControl>
                    <hr/>
                    <FormControl
                        className={classes.formControl}
                    >
                        <Button
                            variant='outlined'
                            disabled={loading}
                            type='submit'
                        >
                            Submit
                            <Icon>
                                submit
                            </Icon>
                        </Button>
                    </FormControl>
                </ValidatorForm>
            </Drawer>
            <Table>
                <thead>
                <tr>
                    <th data-field='id'>ID</th>
                    <th data-field='title'>Name</th>
                    <th data-field='description'>Description</th>
                    <th data-field='quantity'>Quantity</th>
                    <th data-field='price'>Price</th>
                </tr>
                </thead>
                <tbody>
                {products.map(product => {
                    return (
                        <tr>
                            <td>{product.id}</td>
                            <td>{product.title}</td>
                            <td>{product.description}</td>
                            <td>{product.quantity}</td>
                            <td>{product.price}</td>
                            <td>
                                <div>
                                    <div data-product-id={product.id}>
                                        <Button
                                            onClick={e => {
                                                this.handleProductEdit(e, product.id)
                                            }}>
                                            <Icon>edit</Icon>
                                        </Button>
                                        <Button
                                            onClick={e => {
                                                this.handleProductDelete(e, product.id)
                                            }}>
                                            <Icon>delete</Icon>
                                        </Button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </Table>
        </div>
    }
}

export default withStyles(styles)(DashboardProducts)