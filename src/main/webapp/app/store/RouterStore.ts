import {observable, action, reaction} from 'mobx'
import Home from "../components/pages/Home"
import Shopping from "../components/pages/Shopping"
import About from "../components/pages/About"
import SingIn from "../components/pages/SingIn"
import SingUp from "../components/pages/SingUp"
import RouteModel from "app/models/RouteModel";
import userStore from './UserStore'
import Dashboard from "../components/pages/admin/Dashboard";
import DashboardCategories from "app/components/pages/admin/DashboardCategories";
import DashboardProducts from "app/components/pages/admin/DashboardProducts";

class RouterStore {

    private anonymousRouters: Array<RouteModel> = [
        { path: '/', name: 'Home', Component: Home},
        { path: '/singin', name: 'SingIn', Component: SingIn},
        { path: '/about', name: 'About', Component: About},
        { path: '/singup', name: 'SingUp', Component: SingUp}
    ]

    private loggedRoutes: Array<RouteModel> = [
        { path: '/', name: 'Home', Component: Home},
        { path: '/shopping', name: 'Shopping', Component: Shopping},
        { path: '/about', name: 'About', Component: About},
        { path: '/auth:out', name: 'Sing Out', Component: Home}
    ]

    private adminRoutes: Array<RouteModel> = [
        { path: '/', name: 'Home', Component: Home},
        { path: '/shopping', name: 'Shopping', Component: Shopping},
        { path: '/admin', name: 'Dashboard', Component: Dashboard},
        { path: '/admin/categories', name: 'DashboardCategories', Component: DashboardCategories},
        { path: '/admin/products', name: 'DashboardProducts', Component: DashboardProducts},
        { path: '/auth:out', name: 'Sing Out', Component: Home}
    ]

    @observable routes: Array<RouteModel> = this.anonymousRouters

    @action setAnonymousRoutes() {
        this.routes = this.anonymousRouters
    }

    @action setLoggedRoutes() {
        this.routes = this.loggedRoutes
    }

    @action setAdminRoutes() {
        this.routes = this.adminRoutes
    }

    userReaction = reaction(
        () => userStore.user,
        (user) => {
            console.log('reaction - user: ' + user)
            if (user) {
                let singOutRoute

                singOutRoute =
                    this.loggedRoutes.find(route => route['name'].includes('Sing out'))

                singOutRoute['name'] = `Sing out (${userStore.user.name})`
            }
        }
    )

}

export {RouterStore}
export default new RouterStore()