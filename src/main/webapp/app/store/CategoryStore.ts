import {action, isObservableSet, observable} from "mobx";
import Category from 'app/models/CategoryModel'
import commonStore from './CommonStore'


class CategoryStore {
    private HTTP_STATUS_OK: number = 200
    private HTTP_STATUS_CREATED: number = 201

    @observable currentCategory: Category = new Category()
    @observable currentCategoryId: BigInteger = null
    @observable categories: Array<Category> = []

    @action setCategoryName(name: string) {
        this.currentCategory.name = name
    }

    @action setCurrentCategory(category: Category) {
        this.currentCategory = category
    }

    @action setCurrentCategoryId(id: BigInteger) {
        this.currentCategoryId = id
    }

    @action fetchCategories() {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch(commonStore.basename + '/api/categories', {
            method: 'GET'
        }).then((response) => {
            return response.json()
        }).then((responseModel) => {
            if(responseModel) {
                if (responseModel.status === 'success') {
                    this.categories =
                        JSON.parse(
                            decodeURIComponent(
                                JSON.stringify(responseModel.data)
                                    .replace(/( )/ig, "%20")
                            )
                        )
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action add () {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch((commonStore.basename + 'api/categories'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({'name': encodeURIComponent(this.currentCategory.name)})
        }).then((response) => {
            return response.status
        }).then(responseStatusCode => {
            if (responseStatusCode){
                if (responseStatusCode === this.HTTP_STATUS_CREATED) {
                    this.fetchCategories()
                    this.setCurrentCategory(new Category())
                }
            }
        }).catch((error) => {
            commonStore.setError(error.message)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action update () {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch((commonStore.basename + `api/categories/${this.currentCategory.id}`), {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({'name': encodeURIComponent(this.currentCategory.name)})
        }).then((response) => {
            return response.status
        }).then(responseStatusCode => {
            if (responseStatusCode) {
                if (responseStatusCode === this.HTTP_STATUS_OK) {
                    this.fetchCategories()
                    this.setCategoryName('')
                    this.setCurrentCategory(new Category())
                }
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action deleteCategory() {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch(commonStore.basename + 'api/categories/' + this.currentCategoryId, {
            method: 'DELETE'
        }).then((response) => {
            return response.json()
        }).then(responseModel => {
            if (responseModel){
                if (responseModel.status === 'success') {
                    this.fetchCategories()
                    this.setCurrentCategoryId(null)
                    this.setCurrentCategory(new Category())
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }
}
export {CategoryStore}
export default new CategoryStore()