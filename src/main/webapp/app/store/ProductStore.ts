import {action, observable} from "mobx";
import Product from '../models/ProductModel'
import commonStore from "app/store/CommonStore";
import {common} from "@material-ui/core/colors";
import history from "app/history";
import {Simulate} from "react-dom/test-utils";
import error = Simulate.error;

class ProductStore {
    private HTTP_STATUS_OK: number = 200
    private HTTP_STATUS_CREATED: number = 201
    private allowGetPriceBounds: boolean = true
    private allowGetQuantityBounds: boolean = true

    @observable currentProduct: Product = new Product()
    @observable currentProductId: BigInteger = null
    @observable products: Array<Product> = []
    @observable currentProductImage: string = ''
    @observable allowFetchFilteredProducts: boolean = true
    @observable orderBy: string = 'id'
    @observable sortingDirection: string = 'DESC'
    @observable priceFrom: number = null
    @observable priceTo: number = null
    @observable quantityFrom: number = null
    @observable quantityTo: number = null
    @observable categories: Array<number> = []
    @observable searchString: string = ''
    @observable priceFromBound: number = 0
    @observable priceToBound: number = 1000000
    @observable quantityFromBound: number = 0
    @observable quantityToBound: number = 1000000

    @action fetchFilteredProducts() {
        commonStore.clearError()
        commonStore.setLoading(true)
        const fetchFilteredProductsUrl =
            commonStore.basename + `/api/products/filtered
                ::orderBy:${this.orderBy}
                ::sortingDirection:${this.sortingDirection}
                /?search=${this.searchString}`
        fetch(fetchFilteredProductsUrl.replace(/\s/g, ''), {
            method: 'GET'
        }).then((response) => {
            return response.json()
        }).then(responseModel => {
            if (responseModel) {
                if (responseModel.stratus === 'success') {
                    this.products =
                        JSON.parse(
                            decodeURIComponent(
                                JSON.stringify(responseModel.data)
                                    .replace(/(%2)/ig, '%20')
                            )
                        )
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch((error) => {
            commonStore.setError(error.message)
            throw error
        }).finally(action(() => {
            this.setAllowFetchFilteredProducts(true)
            commonStore.setLoading(false)
        }))
    }

    private changeShoppingUrlParams() {
        history.push({
            pathname: '/shopping',
            search: `?orderBy=${this.orderBy}
                        &sortingDirection=${this.sortingDirection}
                        &search=
                            price>:${this.priceFrom}
                            price<:${this.priceTo}
                            quantity>:${this.quantityFrom}
                            quantity<:${this.quantityTo}
                            ${(this.categories && this.categories.length > 0) ? ';categories:' + JSON.stringify(this.categories) : ''}`
                .replace(/\s/g, '')
        })
    }

    private handlePriceBoundValues() {
        if (this.priceFrom && this.priceTo) {
            this.allowGetPriceBounds = false
            setTimeout(() => {
                if (this.allowGetPriceBounds) {
                    this.fetchProductPriceBounds()
                }
            }, 3500)
            this.changeShoppingUrlParams()
        } else {
            this.allowGetPriceBounds = true
            setTimeout(() => {
                if (this.allowGetPriceBounds) {
                    this.fetchProductPriceBounds()
                }
            }, 3500)
        }
    }

    private handleQuantityBoundsValues() {
        if (this.quantityFrom && this.quantityTo) {
            this.allowGetQuantityBounds = false
            setTimeout(() => {
                if (this.allowGetQuantityBounds) {
                    this.fetchProductQuantityBounds()
                }
            }, 3500)
            this.changeShoppingUrlParams()
        } else {
            this.allowGetQuantityBounds = true
            setTimeout(() => {
                if (this.allowGetQuantityBounds) {
                    this.fetchProductQuantityBounds()
                }
            }, 3500)
        }
    }

    @action setCurrentProduct(product: Product) {
        this.currentProduct = product
    }

    @action setCurrentProductId(id: BigInteger) {
        this.currentProductId = id
    }

    @action setProductTitle(title: string) {
        this.currentProduct.title = title
    }

    @action setProductCategory(categoryId: BigInteger) {
        this.currentProduct.categoryId = categoryId
    }

    @action setProductDescription(description: string) {
        this.currentProduct.description = description
    }

    @action setProductPrice(price: number) {
        this.currentProduct.price = price
    }

    @action setProductQuantity(quantity: number) {
        this.currentProduct.quantity = quantity
    }

    @action setProductImage(image: string) {
        this.currentProductImage = image
        this.currentProduct.image = image
    }

    @action setAllowFetchFilteredProducts(allow: boolean) {
        this.allowFetchFilteredProducts = allow
    }

    @action setOrderBy(fieldName: string) {
        this.orderBy = fieldName
        this.changeShoppingUrlParams()
    }

    @action setSortingDirection(direction: string) {
        this.sortingDirection = direction
        this.changeShoppingUrlParams()
    }

    @action setFilterDataPriceFrom(priceFrom: number) {
        this.priceFrom = priceFrom
        this.handlePriceBoundValues()
    }

    @action setFilterDataPriceTo(priceTo: number) {
        this.priceTo = priceTo
        this.handlePriceBoundValues()
    }

    @action setFilterDataQuantityFrom(quantityFrom: number) {
        this.quantityFrom = quantityFrom
        this.handlePriceBoundValues()
    }

    @action setFilterDataQuantityTo(quantityTo: number) {
        this.quantityTo = quantityTo
        this.handlePriceBoundValues()
    }

    @action setFilterDataSearchString(searchString: string) {
        this.searchString = searchString
    }

    @action setFilteredDataCategory(id: number, isChecked: boolean) {
        const categoryId =
            this.categories.find(categoryId => categoryId === id)
        if (!categoryId && isChecked) {
            this.categories.push(id)
        } else if (categoryId && isChecked) {
            this.categories =
                this.categories.filter(categoryId => categoryId !== id)
        }
        this.changeShoppingUrlParams()
    }

    @action fetchProducts() {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch(commonStore.basename + '/api/products', {
            method: 'GET'
        }).then((response) => {
            return response.json()
        }).then(responseModel => {
            if (responseModel) {
                if (responseModel.status === 'success') {
                    this.products =
                        JSON.parse(
                            decodeURIComponent(
                                JSON.stringify(responseModel.data)
                                    .replace(/(%2)/ig, '%20')
                            )
                        )
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action add() {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch(commonStore.basename + '/api/products', {
            method: ' POST',
            headers: {
                'Content-Type': 'application.json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                'title': encodeURIComponent(this.currentProduct.title),
                'description': encodeURIComponent(this.currentProduct.description),
                'price': this.currentProduct.price,
                'quantity': this.currentProduct.quantity,
                'image': this.currentProduct.image,
                'categoryId': this.currentProduct.categoryId
            })
        }).then((response) => {
            return response.status
        }).then(responseStatusCode => {
            if (responseStatusCode) {
                if (responseStatusCode === this.HTTP_STATUS_CREATED) {
                    this.fetchProducts()
                    this.setCurrentProduct(new Product())
                    this.setCurrentProductId(null)
                }
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action update() {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch(commonStore.basename + `/api/products/${this.currentProduct.id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application.json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                'title': encodeURIComponent(this.currentProduct.title),
                'description': encodeURIComponent(this.currentProduct.description),
                'price': this.currentProduct.price,
                'quantity': this.currentProduct.quantity,
                'image': this.currentProduct.image,
                'categoryId': this.currentProduct.categoryId
            })
        }).then((response) => {
            return response.status
        }).then(responseStatusCode => {
            if (responseStatusCode) {
                if (responseStatusCode === this.HTTP_STATUS_CREATED) {
                    this.fetchProducts()
                    this.setProductTitle('')
                    this.setCurrentProduct(new Product())
                    this.setCurrentProductId(null)
                }
            }
        }).catch(error => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action deleteProduct() {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch(commonStore.basename + `/api/products/${this.currentProduct.id}`, {
            method: 'DELETE'
        }).then((response) => {
            return response.json()
        }).then(responseModel => {
            if (responseModel) {
                if (responseModel.status === 'success') {
                    this.fetchProducts()
                    this.setCurrentProduct(new Product())
                    this.setCurrentProductId(null)
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch(error => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action getFilteredProducts() {
        commonStore.clearError()
        commonStore.setLoading(true)

        const filteredProductsUrl = commonStore.basename +
            `/api/products/filtered
                         ::orderBy:${this.orderBy}
                         ::sortingDirection:${this.sortingDirection}
                         /?search=
                         price>:${this.priceFrom}
                         price<:${this.priceTo}
                         ${(this.categories && this.categories.length > 0) ? ';categories:' + JSON.stringify(this.categories) : ''}`
        console.log(filteredProductsUrl)
        fetch(filteredProductsUrl.replace(/\s/g, ''), {
            method: 'GET'
        }).then(response => {
            return response.json()
        }).then(responseModel => {
            if(responseModel) {
                if(responseModel.status === 'success') {
                    this.products =
                        JSON.parse(
                            decodeURIComponent(
                                JSON.stringify(responseModel.data)
                                    .replace(/(%2)/ig, '%20')
                            )
                        )
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch(error => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action fetchProductQuantityBounds() {
        commonStore.clearError()
        commonStore.setLoading(true)

        fetch(commonStore.basename + '/api/products/quantity-bound', {
            method: 'GET'
        }).then(response => {
            return response.json()
        }).then(responseModel => {
            if(responseModel) {
                if(responseModel.status === 'success') {
                    this.quantityFromBound = responseModel.data.min
                    this.quantityToBound = responseModel.data.max
                    if(this.allowGetQuantityBounds){
                        if(!this.quantityFrom) {
                            this.quantityFrom = this.quantityFromBound
                        }
                        if(!this.quantityTo) {
                            this.quantityTo = this.quantityToBound
                        }
                        this.changeShoppingUrlParams()
                    }
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch(error => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action fetchProductPriceBounds() {
        commonStore.clearError()
        commonStore.setLoading(true)

        fetch(commonStore.basename + '/api/products/price-bound', {
            method: 'GET'
        }).then(response => {
            return response.json()
        }).then(responseModel => {
            if(responseModel) {
                if(responseModel.status === 'success') {
                    this.priceFromBound = responseModel.data.min
                    this.priceToBound = responseModel.data.max
                    if(this.allowGetPriceBounds){
                        if(!this.priceFrom) {
                            this.priceFrom = this.priceFromBound
                        }
                        if(!this.priceTo) {
                            this.priceTo = this.priceToBound
                        }
                        this.changeShoppingUrlParams()
                    }
                } else if (responseModel.status === 'fail') {
                    commonStore.setError(responseModel.message)
                }
            }
        }).catch(error => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }
}
export {ProductStore}
export  default new ProductStore()
