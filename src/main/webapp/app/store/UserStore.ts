import User from "app/models/UserModel";
import {observable, action} from 'mobx';
import commonStore from "../store/CommonStore";

class UserStore {
    readonly HTTP_STATUS_OK: number = 200

    @observable user: User = null
    @observable userName: string = ''
    @observable password: string = ''

    @action setUser(user: User) {
        this.user = user
    }

    @action setUserName(userName: string) {
        this.userName = userName
    }

    @action setPassword(password: string) {
        this.password = password
    }

    @action reset() {
        this.userName = ''
        this.password = ''
    }

    @action check() {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch('api/auth/user/check', {
            method: 'GET'
        }).then((response) => {
            return response.json()
        }).then((response) => {
            if (response) {
                if (response.status === 'success') {
                    this.user = new User(response.data.name, response.data.roleName)
                } else if (response.status === 'fail') {
                    commonStore.setError(response.message)
                }
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action login () {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch('login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: `username=${this.userName}&password=${this.password}`
        }).then((response) => {
            return response.status
        }).then((statusCode) => {
            if(statusCode == this.HTTP_STATUS_OK) {
                this.check()
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action logout () {
        commonStore.setLoading(true)
        fetch('logout', {
            method: 'Get'
        }).then((response) => {
            return response.json()
        }).then((response) => {
            if(response) {
                if (response.status === 'success') {
                    this.user = null
                } else if (response.status === 'fail') {
                    commonStore.setError(response.message)
                }
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }

    @action register () {
        commonStore.clearError()
        commonStore.setLoading(true)
        fetch('api/auth/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({'name': this.userName, 'password': this.password})
        }).then((response) => {
            return response.json()
        }).then((response) => {
            if(response.status == 'success'){
                this.login()
            } else {
                commonStore.setError(response.message)
            }
        }).catch((error) => {
            commonStore.setError(error)
            throw error
        }).finally(action(() => {
            commonStore.setLoading(false)
        }))
    }
}
export {UserStore}
export default new UserStore()