package org.dmdev.springbootreactjs.application.repositories.predicate;

import org.dmdev.springbootreactjs.SpringbootReactjsApplication;
import org.dmdev.springbootreactjs.entities.Product;
import org.dmdev.springbootreactjs.repositories.ProductHibernateDAO;
import org.dmdev.springbootreactjs.repositories.predicate.ProductPredicatesBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE,
        classes = SpringbootReactjsApplication.class
)
public class ProductQueryDslTest {
    @Autowired
    private ProductHibernateDAO productDAO;

    @Test
    public void givenName_whenGettingListOfORCLProducts_thenCorrect() {
        ProductPredicatesBuilder builder =
                new ProductPredicatesBuilder().with("name", ":", "ORCL");
        List<Product> products =
                (List<Product>) productDAO.findAll(builder.build());
        assertNotNull(products);
        assertEquals(2, products.size());
        products.forEach(product -> assertEquals("ORCL", product.getName()));
    }

    @Test
    public void givenNameAndQuantity_whenGettingListOfORCLProducts_thenCorrect() {
        ProductPredicatesBuilder builder =
                new ProductPredicatesBuilder()
                        .with("name", ":", "ORCL")
                        .with("quantity", ":", "1500");
        List<Product> products =
                (List<Product>) productDAO.findAll(builder.build());
        assertNotNull(products);
        assertEquals(1, products.size());
        products.forEach(product -> {
                    assertEquals("ORCL", product.getName());
                    assertTrue(product.getQuantity() > 1500);
                }
        );
    }
}
