package org.dmdev.springbootreactjs.application.controller;

import org.dmdev.springbootreactjs.controllers.AuthController;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.class)
public class SecurityControllerMethodsTest {
    @Autowired
    public AuthController authController;

    @Test
    @Order(1)
    public void shouldThrowAuthenticationCredentialsNotFoundException() {
        assertThrows(AuthenticationCredentialsNotFoundException.class, () -> {
            authController.getAllRoles();
        });
    }

    @Test
    @WithUserDetails(
            value = "admin",
            userDetailsServiceBeanName = "hibernateWebAuthProvider"
    )
    @Order(2)
    public void withUserDetailsTest(){}

    @Test
    @WithMockUser(username = "admin", roles = {"ADMIN"})
    @Order(3)
    public void getAllRolesByAdminUserTest() {
        ResponseEntity responseEntity = authController.getAllRoles();
        assertNotNull(responseEntity);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    @Order(4)
    public void getAllRolesByAdminRoleTest() {
        ResponseEntity responseEntity = authController.getAllRoles();
        assertNotNull(responseEntity);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }
}
