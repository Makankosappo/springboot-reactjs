package org.dmdev.springbootreactjs.application.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Lists;
import org.dmdev.springbootreactjs.SpringbootReactjsApplication;
import org.dmdev.springbootreactjs.models.ProductModel;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = SpringbootReactjsApplication.class
)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.class)
public class ProductControllerRequestTest {
    @Autowired
    private TestRestTemplate testRestTemplate;
    final String baseUrl = "/api";
    
    @Test
    @Order(1)
    public void givenNameAndQuantity_whenRequestsListOfORCLProducts_thenCorrect() throws Exception{
        ResponseEntity<ResponseModel> response =
                testRestTemplate.getForEntity(
                        baseUrl + "/products/filtered::orderBy:id::sortingDirection:DESC/?search=name:ORCL;quantity>1500",
                        ResponseModel.class
                );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        ArrayList products =
                (ArrayList) response.getBody().getData();
        assertNotNull(products);
        assertEquals(1, products.size());
        List<ProductModel> productModels =
                (new ObjectMapper())
                        .convertValue(products, new TypeReference<List<ProductModel>>() { });
        productModels.forEach(product -> {
            assertEquals("ORCL", product.getTitle());
            assertTrue(product.getQuantity() > 1500);
        });
    }

    @Test
    @Order(2)
    public void givenCategoryIdAndQuantity_whenRequestsListOfProducts_thenCorrect(){
        ResponseEntity<ResponseModel> response =
                testRestTemplate.getForEntity(
                        baseUrl + "/products/filtered::orderBy:id::sortingDirection:DESC/?search=category:[1, 2];quantity>1500",
                        ResponseModel.class
                );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        ArrayList products =
                (ArrayList) response.getBody().getData();
        assertNotNull(products);
        assertEquals(1, products.size());
        List<ProductModel> productModels =
                (new ObjectMapper())
                        .convertValue(products, new TypeReference<List<ProductModel>>() { });
        productModels.forEach(product -> {
            List<Long> categoriesIds = Lists.newArrayList(1L, 2L);
            Matcher<Iterable<? super Long>> matcher = hasItem(product.getCategory().getId());
            assertThat(categoriesIds, matcher);
        });
    }
}
