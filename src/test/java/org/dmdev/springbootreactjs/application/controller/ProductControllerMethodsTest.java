package org.dmdev.springbootreactjs.application.controller;

import org.dmdev.springbootreactjs.controllers.ProductController;
import org.dmdev.springbootreactjs.models.ProductFilterModel;
import org.dmdev.springbootreactjs.models.ProductModel;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.util.AssertionErrors.fail;

@SpringBootTest
public class ProductControllerMethodsTest {
    private Long lastId = Long.MAX_VALUE;
    @Autowired
    private ProductController productController;

    @Test
    public void shouldFilteredProductByCategories() {
        final ProductFilterModel filter =
                ProductFilterModel.builder()
                        .categories(Arrays.asList(1L, 2L))
                        .orderBy("id")
                        .sortingDirection(Sort.Direction.DESC)
                        .build();
        ResponseEntity responseEntityFiltered =
                productController.getByCategories(
                        filter.categories,
                        filter.orderBy,
                        filter.sortingDirection
                );
        assertNotNull(responseEntityFiltered);
        assertEquals(responseEntityFiltered.getStatusCode(), HttpStatus.OK);
        ((List<ProductModel>) ((ResponseModel) responseEntityFiltered.getBody())
                .getData())
                .forEach(productModel -> {
                    if (!(productModel.getCategory().getId().equals(1L)
                            || productModel.getCategory().getId().equals(2L))) {
                        fail("Excepted Category id equals 1L or 2L, but got " + productModel.getCategoryId());
                    }
                    if (productModel.getId() > lastId) {
                        fail("Excepted DESC sort, but got ASC one");
                    }
                    lastId = productModel.getId();
                });
    }
}
