package org.dmdev.springbootreactjs.application.controller;

import org.dmdev.springbootreactjs.SpringbootReactjsApplication;
import org.dmdev.springbootreactjs.security.HibernateWebAuthProvider;
import org.dmdev.springbootreactjs.security.RestAuthenticationEntryPoint;
import org.dmdev.springbootreactjs.security.SavedReqAwareAuthSuccessHandler;
import org.dmdev.springbootreactjs.security.SecurityConfig;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.print.DocFlavor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = SpringbootReactjsApplication.class
)
@AutoConfigureMockMvc
@ContextConfiguration(classes = {
        HibernateWebAuthProvider.class,
        RestAuthenticationEntryPoint.class,
        SavedReqAwareAuthSuccessHandler.class,
        SecurityConfig.class
})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.class)
public class SecurityControllerRequestsTest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private TestRestTemplate testRestTemplate;
    final String baseUrl = "http://localhost:" + 8092 + "/";

    @Test
    @Order(1)
    public void performLoginDefault() throws Exception {
        mvc.perform(formLogin("/login"))
                .andExpect((redirectedUrl("/api/auth/user/onerror")));
    }

    @Test
    @Order(2)
    public void performLoginWithAdminUserPassword() throws Exception {
        mvc.perform(formLogin("/login")
                        .user("admin")
                        .password("AdminPassword"))
                .andExpect((status().isOk()));
    }

    @Test
    @Order(3)
    public void performLoginWithWrongUserPassword() throws Exception {
        mvc.perform(formLogin("/login")
                        .user("username", "admin")
                        .password("password", "WrongPassword1"))
                .andExpect((redirectedUrl("/api/auth/user/onerror")));
    }

    @Test
    @Order(4)
    public void performLogout() throws Exception {
        mvc.perform(get("/logout"))
                .andExpect((redirectedUrl("/api/auth/user/signedout")));
    }

    @Test
    @Order(5)
    public void whenAdminRequestAllRoles_thenSuccess() throws Exception {
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        baseUrl + "api/auth/admin/roles",
                        HttpMethod.GET,
                        new HttpEntity<String>(loginAdmin()),
                        String.class
                );
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @Order(6)
    public void whenLoggedUserRequestsAllRoles_thenForbidden() {
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        baseUrl + "api/auth/admin/roles",
                        HttpMethod.GET,
                        new HttpEntity<String>(loginUser()),
                        String.class
                );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    @Test
    @Order(7)
    public void whenAnyUserRequestsIndexPage_thenSuccess() {
        ResponseEntity<String> response =
                testRestTemplate.getForEntity(baseUrl, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @Order(8)
    public void whenAnonymousUserRequestsDeleteUser_thenUnauthorized() {
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        baseUrl + "api/auth/user/2",
                        HttpMethod.DELETE,
                        new HttpEntity<String>(anonymousUser()),
                        String.class
                );
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    @Order(9)
    public void whenLoggedUserRequestsDeleteUser_thenSuccess() {
        ResponseEntity<String> response =
                testRestTemplate.exchange(
                        baseUrl + "api/auth/user/2",
                        HttpMethod.DELETE,
                        new HttpEntity<String>(loginUser()),
                        String.class
                );
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    private HttpHeaders login(String username, String password) {
        MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
        request.set("username", username);
        request.set("password", password);
        ResponseEntity<String> response =
                this.testRestTemplate.withBasicAuth(username, password)
                        .postForEntity("/login", request, String.class);
        HttpHeaders headers = response.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders requestHttpHeaders = new HttpHeaders();
        requestHttpHeaders.set("Cookie", cookie);
        return requestHttpHeaders;
    }

    private HttpHeaders loginAdmin() {
        return login("admin", "AdminPassword1");
    }

    private HttpHeaders loginUser() {
        return login("one", "UserPassword1");
    }

    private HttpHeaders anonymousUser() {
        return new HttpHeaders();
    }
}
