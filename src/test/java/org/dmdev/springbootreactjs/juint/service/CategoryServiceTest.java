package org.dmdev.springbootreactjs.juint.service;

import org.apache.commons.lang3.builder.ToStringExclude;
import org.dmdev.springbootreactjs.entities.Category;
import org.dmdev.springbootreactjs.models.CategoryModel;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.models.RoleModel;
import org.dmdev.springbootreactjs.repositories.CategoryHibernateDAO;
import org.dmdev.springbootreactjs.services.CategoryService;
import org.dmdev.springbootreactjs.services.interfaces.ICategoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {
    @Mock
    private CategoryHibernateDAO categoryDAO;
    @Mock
    private ICategoryService categoryServiceMock;
    @InjectMocks
    private CategoryService categoryService;

    ArgumentCaptor<Category> categoryArgument =
            ArgumentCaptor.forClass(Category.class);

    @Test
    void shouldCreatedCategoryReturnSuccessfully(){
        final CategoryModel category =
                CategoryModel.builder()
                        .name("Test Category 1")
                        .build();
        ResponseModel responseModel =
                categoryService.create(category);

        assertNotNull(responseModel);
        assertEquals(ResponseModel.SUCCESS_STATUS, responseModel.getStatus());
        verify(categoryDAO, atLeast(1)).save(categoryArgument.capture());
    }

    @Test
    void shouldThrowConstraintException(){
        final String tooLongCategoryName =
                "test category 1234567890 1234567890 1234567890";
        final CategoryModel tooLongCategoryNameModel = CategoryModel.builder()
                .name(tooLongCategoryName)
                .build();
        given(categoryServiceMock.create(tooLongCategoryNameModel))
                .willThrow(new IllegalArgumentException());

        try {
            final CategoryModel categoryModel =
                    CategoryModel.builder()
                            .name(tooLongCategoryName)
                            .build();
            categoryServiceMock.create(categoryModel);
            fail("Should throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {
            then(categoryDAO)
                    .should(never())
                    .save(categoryArgument.capture());
        }
    }

    @Test
    @ExtendWith(SystemOutResource.class)
    void checkSuccessLogging(/*SystemOutResource sysOut*/){
        final CategoryModel categoryModel =
                CategoryModel.builder()
                        .name("Test Category 1")
                        .build();
        categoryService.create(categoryModel);
        assertEquals(
                String.format("Category %s Created", categoryModel.getName().trim()),
                SystemOutResource.outContent.toString().trim()
        );
    }
}
