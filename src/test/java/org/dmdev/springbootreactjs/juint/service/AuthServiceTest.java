package org.dmdev.springbootreactjs.juint.service;

import org.dmdev.springbootreactjs.entities.Role;
import org.dmdev.springbootreactjs.models.ResponseModel;
import org.dmdev.springbootreactjs.models.RoleModel;
import org.dmdev.springbootreactjs.repositories.RoleHibernateDAO;
import org.dmdev.springbootreactjs.repositories.UserHibernateDAO;
import org.dmdev.springbootreactjs.services.AuthService;
import org.dmdev.springbootreactjs.services.interfaces.IAuthService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;


import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {
    @Mock
    private UserHibernateDAO userDAO;
    @Mock
    private RoleHibernateDAO roleDAO;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private IAuthService authServiceMock;
    @InjectMocks
    private AuthService authService;

    ArgumentCaptor<Role> roleArgument =
            ArgumentCaptor.forClass(Role.class);

    @Test
    void shouldCreatedRoleReturnSuccessfully() {
        final RoleModel role =
                RoleModel.builder()
                        .name("ROLE_DEMO")
                        .build();
        ResponseModel responseModel =
                authService.createRole(role);

        assertNotNull(responseModel);
        assertEquals(ResponseModel.SUCCESS_STATUS, responseModel.getStatus());
        verify(roleDAO, atLeast(1)).save(roleArgument.capture());
    }

    @Test
    void shouldGetAllRolesReturnSuccessfully() {
        doReturn(
                ResponseModel.builder()
                        .status(ResponseModel.SUCCESS_STATUS)
                        .data(Arrays.asList(
                                new RoleModel(1L, "ROLE_DEMO1"),
                                new RoleModel(2L, "ROLE_DEMO2"),
                                new RoleModel(3L, "ROLE_DEMO3")
                        )).build()
        ).when(authServiceMock)
                .getAllRoles();
        ResponseModel responseModel =
                authServiceMock.getAllRoles();
        assertNotNull(responseModel);
        assertNotNull(responseModel.getData());
        assertEquals( ((List)responseModel.getData()).size(), 3);
    }
}
